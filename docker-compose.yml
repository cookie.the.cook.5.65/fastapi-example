version: "3.9"

services:
  fastapi:
    build:
      dockerfile: docker/fastapi/Dockerfile
      target: development
    container_name: example-app-fastapi
    depends_on:
      db:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
      redis:
        condition: service_started
    ports:
      - 8000:80
    volumes:
      - .:/app
    env_file: .env
    networks:
      - example-app

  celery-worker:
    build:
      dockerfile: docker/celery/Dockerfile
      target: celery-worker
    container_name: celery-worker
    depends_on:
      db:
        condition: service_healthy
      rabbitmq:
        condition: service_healthy
      redis:
        condition: service_started
    volumes:
      - .:/app
    env_file: .env
    networks:
      - example-app

  celery-beat:
    build:
      dockerfile: docker/celery/Dockerfile
      target: celery-beat
    container_name: celery-beat
    depends_on:
      rabbitmq:
        condition: service_healthy
    volumes:
      - .:/app
    env_file: .env
    networks:
      - example-app

  flower:
    image: mher/flower:1.2
    container_name: flower
    ports:
      - 5555:5555
    environment:
      - FLOWER_BROKER_API=http://guest:guest@rabbitmq:15672/api/
    volumes:
      - flower_data:/data
    networks:
      - example-app

  celery-exporter:
    image: danihodovic/celery-exporter:0.7.6
    container_name: celery-exporter
    environment:
      - CE_BROKER_URL=amqp://guest:guest@rabbitmq:5672/
    ports:
      - 9808:9808
    depends_on:
      rabbitmq:
        condition: service_healthy
    networks:
      - example-app

  db:
    image: postgres:15
    container_name: db
    ports:
      - 5432:5432
    restart: always
    volumes:
      - db_data:/var/lib/postgresql/data
    environment:
      - POSTGRES_DB=fastapi_example
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=postgres
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "pg_isready -U $$POSTGRES_USER -d $$POSTGRES_DB"
        ]
      interval: 1s
      timeout: 1s
      retries: 10
    networks:
      - example-app

  rabbitmq:
    image: rabbitmq:3-management
    container_name: rabbitmq
    ports:
      - 15672:15672
    healthcheck:
      test: [ "CMD", "rabbitmq-diagnostics", "check_running" ]
      interval: 10s
      timeout: 10s
      retries: 10
    volumes:
      - rabbitmq_data:/var/lib/rabbitmq
    networks:
      - example-app

  redis:
    image: redis:7
    container_name: redis
    volumes:
      - redis_data:/data
    networks:
      - example-app

  prometheus:
    image: prom/prometheus:v2.44.0
    restart: unless-stopped
    container_name: prometheus
    ports:
      - 9090:9090
    volumes:
      - ./docker/prometheus/prometheus.yml:/etc/prometheus/prometheus.yml
      - prometheus_data:/prometheus
    command:
      - "--config.file=/etc/prometheus/prometheus.yml"
    networks:
      - example-app

  grafana:
    image: grafana/grafana:9.5.2
    restart: unless-stopped
    user: "472"
    container_name: grafana
    depends_on:
      - prometheus
    ports:
      - 3030:3000
    volumes:
      - ./docker/grafana/datasource.yml:/etc/grafana/provisioning/datasources/datasource.yml
      - ./docker/grafana/dashboards.yml:/etc/grafana/provisioning/dashboards/dashboards.yml
      - ./docker/grafana/dashboards/:/var/lib/grafana/dashboards/
    environment:
      - GF_SECURITY_ADMIN_PASSWORD=password
      - GF_USERS_ALLOW_SIGN_UP=false
    networks:
      - example-app

volumes:
  db_data:
  rabbitmq_data:
  redis_data:
  prometheus_data:
  flower_data:


networks:
  example-app:
    external: true
