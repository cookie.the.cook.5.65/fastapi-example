# FastAPI app example
Пример приложения на FastAPI для создания заметок + Celery
## Фичи
* Регистрация
* Вход
* Список публичных заметок
* Список собственных заметок
* Список заметок пользователя
* Постраничная навигация
* Поиск заметок
* Создание, просмотр, редактирование и удаление заметок
## Технические фичи
* Python 3.11
* Poetry для управления зависимостями
* ORM SQLAlchemy 2.0 в асинхронном режиме
* Alembic для миграций (интегрирован с асинхронной SQLAlchemy), автоматическое именование миграций по дате и времени создания
* Автоматическая генерация имен таблиц бд в snake case
* Типипзированный дженерик репозиторий для CRUD SQLAlchemy, репозиторий для safe delete (записи помечаются удаленнымми)
* Слой сервисов для бизнес логики
* orjson для быстрой сериализации и десериализации json, интегрирован с pydantic моделями и SQLAlchemy
* Группы настроек для разных окружений (development, test, staging, production)
* Настройки CORS
* API документация отключена для production окружения
* Sentry для staging и production окружений
* fastapi-users для управления пользователями и аутентификации
* sqladmin – админка, аутентификация интегрирована с fastapi-users
* fastapi-pagination для api пагинации
* fastapi-filter для api фильтрации
* Healthcheck
* prometheus-fastapi-instrumentator – метрики для prometheus
* Celery - 1 prefork worker + celery beat (запуск по расписаниию). Broker - rabbitmq, result backed - redis. Flower - для мониторинга и ууправления. celery-exporter - метрики celery для prometheus + 2 дашборда в grafana. Api для запуска любой задачи (суперпользователь), api для получения результата задачи (без аутентификации).
* Пре-коммит хуки: ruff, isort, black
* mypy
* fastapi-debug-toolbar – для отладки и профайлинга
* Логирование sql запросов в dev окружении
* pytest, pytest-cov – тесты (настроены для работы асинхронного кода) и отчеты о покрытии
* factory-boy – генерация случайных тестовых данных, интегрирован с асинхронной SQLAlchemy
* pytest-alembic для тестирования миграций
* docker compose для локального dev окружения, multi-stage build dockerfile, удаленная отладка в докере, отдельный stage (target) для сборки production образа
## Запуск локально в docker
### Создать файл с переменными окружения
```console
$ cp .env.example .env
```
### Создать внешнюю сеть для бэкенда и фронтенда (если ещё не создана)
```console
$ docker network create example-app
```
### Запустить проект
```console
$ docker compose up --build -d
```
### Создать базу данных (применить миграции)
```console
$ docker compose exec fastapi alembic upgrade head
```
### Проверить работоспособность сервиса
```console
$ curl --silent http://127.0.0.1:8000/healthcheck | jq
{
  "database": "healthy",
  "rabbitmq": "healthy",
  "redis": "healthy",
  "status": "healthy"
}
```
## Использование
### API
* Swagger [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)
* Redoc [http://127.0.0.1:8000/redoc](http://127.0.0.1:8000/redoc)
### Админка
[http://127.0.0.1:8000/admin](http://127.0.0.1:8000/admin)
Для доступа необходимо создать суперпользователя
```console
$ docker compose exec fastapi python scripts/create_superuser.py
```
### Метрики
* Grafana (admin/password)
  * FastAPI [http://127.0.0.1:3030/d/example-app/fastapi](http://127.0.0.1:3030/d/example-app/fastapi)
  * Celery tasks by task [http://127.0.0.1:3030/d/celery-tasks-by-task-32s3/celery-tasks-by-task](http://127.0.0.1:3030/d/celery-tasks-by-task-32s3/celery-tasks-by-task)
  * Celery tasks overview [http://127.0.0.1:3030/d/celery-tasks-by-task-32s3/celery-tasks-overview](http://127.0.0.1:3030/d/celery-tasks-by-task-32s3/celery-tasks-overview)
* Prometheus [http://127.0.0.1:9090/graph](http://127.0.0.1:9090/graph)
### RabbitMQ
[http://127.0.0.1:15672/](http://127.0.0.1:15672/) (guest/guest)
## Разработка
### Установить пре-коммит хуки
```console
$ pip install pre-commit
$ pre-commit install
```
### Создание миграций
```console
$ docker compose exec fastapi alembic revision --autogenerate -m "migration_name"
```
### Применение миграций
```console
$ docker compose exec fastapi alembic upgrade head
```
### Запуск тестов
```console
$ docker compose exec fastapi pytest
```
Отчет о покрытии доступен в `htmlcov/index.html`
### Запуск для удаленной отладки в докере
```console
$ docker compose -f docker-compose.yml -f docker-compose.debug.yml up --build -d
```
После этого можно подключаться дебагером из иде к 5678 порту
## Сборка production образа
```console
$ docker build -t fastapi-example:0.1.0 --target production .
```
