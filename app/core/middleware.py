from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.core.config import settings


def add_middlewares(app: FastAPI) -> None:
    if settings.allowed_cors_origins:
        app.add_middleware(
            CORSMiddleware,
            allow_origins=settings.allowed_cors_origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )

    if settings.debug and settings.debug_toolbar:
        from app.utils.debug_toolbar import DebugToolbarMiddleware

        app.add_middleware(DebugToolbarMiddleware, settings=[settings])
