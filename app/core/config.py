import os
from enum import StrEnum, auto
from functools import lru_cache
from typing import Literal

import dotenv
from pydantic import (
    AnyHttpUrl,
    AnyUrl,
    BaseSettings,
    Field,
    HttpUrl,
    PostgresDsn,
    SecretStr,
)


class AppEnv(StrEnum):
    development = auto()
    test = auto()
    staging = auto()
    production = auto()


class SentryEnv(StrEnum):
    staging = auto()
    production = auto()


class Settings(BaseSettings):
    env: AppEnv
    project_name: str = "FastAPI example"
    docs_url: str = "/docs"
    redoc_url: str = "/redoc"
    show_api_docs: bool = True
    openapi_prefix: str = ""
    open_api_url: str | None = "/openapi.json"
    version: str = "0.1.0"

    secret: SecretStr

    api_v1_prefix: str = "/api/v1"

    jwt_token_lifetime_seconds: int = 7 * 24 * 60 * 60  # 7 days

    postgres_host: str
    postgres_user: str
    postgres_password: SecretStr
    postgres_db: str
    database_pool_size: int = 5

    debug: bool = False
    debug_toolbar: bool = False
    log_sql: bool = False

    pagination_page_size: int = 10

    # e.g: ALLOWED_CORS_ORIGINS=["http://localhost", "http://localhost:3000"]
    allowed_cors_origins: list[AnyHttpUrl | Literal["*"]] = []

    celery_broker_url: AnyUrl
    celery_result_backend: AnyUrl

    metrics_enabled: bool = True

    sentry_enabled: bool = False
    sentry_dsn: HttpUrl | None
    sentry_env: SentryEnv | None
    sentry_traces_sample_rate: float = Field(default=1.0, ge=0.0, le=1.0)

    @property
    def fastapi_kwargs(self) -> dict:
        return {
            "title": self.project_name,
            "debug": self.debug,
            "docs_url": self.docs_url,
            "redoc_url": self.redoc_url,
            "openapi_prefix": self.openapi_prefix,
            "open_api_url": self.open_api_url if self.show_api_docs else None,
            "version": self.version,
        }

    @property
    def sentry_kwargs(self) -> dict:
        return {
            "dsn": self.sentry_dsn,
            "environment": self.sentry_env,
            "traces_sample_rate": self.sentry_traces_sample_rate,
        }

    @property
    def database_url(self) -> PostgresDsn:
        return PostgresDsn.build(
            scheme="postgresql+asyncpg",
            user=self.postgres_user,
            password=self.postgres_password.get_secret_value(),
            host=self.postgres_host,
            path=f"/{self.postgres_db}",
        )


class DevelopmentSettings(Settings):
    env = AppEnv.development
    secret = SecretStr("secret")
    debug = True
    debug_toolbar = True
    log_sql = True
    allowed_cors_origins: list[AnyHttpUrl | Literal["*"]] = ["*"]


class TestSettings(Settings):
    env = AppEnv.test
    secret = SecretStr("secret")


class StagingSettings(Settings):
    env = AppEnv.staging
    sentry_enabled = True
    sentry_env = SentryEnv.staging


class ProductionSettings(Settings):
    env = AppEnv.production
    show_api_docs = False
    sentry_enabled = True
    sentry_env = SentryEnv.production


env_to_settings = {
    AppEnv.development: DevelopmentSettings,
    AppEnv.test: TestSettings,
    AppEnv.staging: StagingSettings,
    AppEnv.production: ProductionSettings,
}


@lru_cache
def get_settings() -> Settings:
    dotenv.load_dotenv()
    env = AppEnv(os.getenv("ENV", ""))
    Settings = env_to_settings[env]
    return Settings()


settings: Settings = get_settings()
