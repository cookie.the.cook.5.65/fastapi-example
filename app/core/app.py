import logging

from fastapi import FastAPI
from fastapi_pagination import add_pagination

from app.admin.config import add_admin
from app.api.healthcheck import router as healthcheck_router
from app.api.v1.router import router as api_v1_router
from app.core.config import Settings
from app.core.middleware import add_middlewares
from app.utils.metrics import add_metrics
from app.utils.sentry import init_sentry


def create_app(settings: Settings) -> FastAPI:
    logging.basicConfig(level=logging.INFO)

    init_sentry()

    app = FastAPI(**settings.fastapi_kwargs)

    app.include_router(api_v1_router, prefix=settings.api_v1_prefix)
    app.include_router(healthcheck_router)

    add_middlewares(app)
    add_admin(app)
    add_pagination(app)
    add_metrics(app)

    return app
