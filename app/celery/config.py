from app.core.config import settings

broker_url = settings.celery_broker_url
result_backend = settings.celery_result_backend

imports = ("app.celery.tasks",)

worker_send_task_events = True
task_send_sent_event = True

beat_schedule = {
    "example_periodic_task_every_15s": {
        "task": "example.example_periodic_task",
        "schedule": 15,
    },
}
