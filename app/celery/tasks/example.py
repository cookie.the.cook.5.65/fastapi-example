from app.celery.app import celery_app


@celery_app.task(name="example.example_task")
def example_task() -> dict:
    return {"result": "success"}


@celery_app.task(name="example.example_periodic_task")
def example_periodic_task() -> dict:
    return {"result": "success"}
