import uuid
from typing import Generic, Literal, Type, TypeVar

from sqlalchemy import delete, func, select, update
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.sql.expression import Select, UnaryExpression

from app.database.base import IDBase, IDSafeDeleteBase

ModelType = TypeVar("ModelType", bound=IDBase)
ModelSafeDeleteType = TypeVar("ModelSafeDeleteType", bound=IDSafeDeleteBase)
PKType = TypeVar("PKType", int, uuid.UUID)


class BaseRepository(Generic[ModelType, PKType]):
    model: Type[ModelType]
    order_by: UnaryExpression | None = None

    def __init__(self, db: AsyncSession) -> None:
        self.db = db

    def get_query(self) -> Select:
        return select(self.model).order_by(self.order_by)

    async def all(self) -> list[ModelType]:
        results = await self.db.scalars(self.get_query())
        return list(results.all())

    async def get(self, pk: PKType) -> ModelType | None:
        query = self.get_query().filter(self.model.id == pk).limit(1)
        result = await self.db.scalars(query)
        return result.one_or_none()

    async def create(self, fields: dict, commit: bool = True) -> ModelType:
        db_obj = self.model(**fields)
        self.db.add(db_obj)
        if commit:
            await self.db.commit()
            await self.db.refresh(db_obj)
        return db_obj

    async def update(
        self, db_obj: ModelType, fields: dict, commit: bool = True
    ) -> ModelType:
        for k, v in fields.items():
            setattr(db_obj, k, v)
        self.db.add(db_obj)
        if commit:
            await self.db.commit()
            await self.db.refresh(db_obj)
        return db_obj

    async def delete(self, pk: PKType, commit: bool = True) -> Literal[True]:
        query = delete(self.model).where(self.model.id == pk)
        await self.db.execute(query)
        if commit:
            await self.db.commit()
        return True


class SafeDeleteRepository(BaseRepository[ModelSafeDeleteType, PKType]):
    model: Type[ModelSafeDeleteType]

    def get_query(self) -> Select:
        return super().get_query().filter(self.model.deleted_at.is_(None))

    async def delete(self, pk: PKType, commit: bool = True) -> Literal[True]:
        query = (
            update(self.model).where(self.model.id == pk).values(deleted_at=func.now())
        )
        await self.db.execute(query)
        if commit:
            await self.db.commit()
        return True
