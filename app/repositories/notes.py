import uuid

from sqlalchemy.orm import joinedload
from sqlalchemy.sql.expression import Select

from app import models
from app.repositories.base import SafeDeleteRepository


class NotesRepository(
    SafeDeleteRepository[models.Note, int],
):
    model = models.Note

    def get_query(self) -> Select:
        return super().get_query().options(joinedload(models.Note.owner))

    def get_public_notes_query(self) -> Select:
        return (
            self.get_query()
            .filter(self.model.is_public.is_(True))
            .order_by(models.Note.created_at.desc())
        )

    def get_owner_notes_query(self, owner_id: uuid.UUID) -> Select:
        return (
            self.get_query()
            .filter(self.model.owner_id == owner_id)
            .order_by(models.Note.updated_at.desc())
        )
