import orjson
from sqlalchemy.engine import URL
from sqlalchemy.ext.asyncio import AsyncEngine, async_sessionmaker, create_async_engine

from app.core.config import settings
from app.utils.orjson import orjson_dumps


def get_engine(
    url: str | URL, log_sql: bool = False, pool_size: int = 5
) -> AsyncEngine:
    return create_async_engine(
        url,
        echo=log_sql,
        pool_size=pool_size,
        json_serializer=orjson_dumps,
        json_deserializer=orjson.loads,
    )


def get_session_factory(engine: AsyncEngine) -> async_sessionmaker:
    return async_sessionmaker(engine, expire_on_commit=False)


engine = get_engine(
    url=settings.database_url,
    log_sql=settings.log_sql,
    pool_size=settings.database_pool_size,
)

session_factory = get_session_factory(engine=engine)
