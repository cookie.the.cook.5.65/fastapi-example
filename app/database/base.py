import datetime
import uuid

import stringcase
from sqlalchemy import DateTime
from sqlalchemy.orm import DeclarativeBase, Mapped, declared_attr, mapped_column


class Base(DeclarativeBase):
    @declared_attr  # type: ignore
    def __tablename__(cls) -> str:
        return stringcase.snakecase(cls.__name__)


class IDBase(Base):
    __abstract__ = True

    id: Mapped[int | uuid.UUID]


class IDSafeDeleteBase(IDBase):
    __abstract__ = True

    deleted_at: Mapped[datetime.datetime | None] = mapped_column(
        DateTime, nullable=True, index=True
    )
