from sqlalchemy import text
from sqlalchemy.engine import URL

from app.database import get_engine


async def create_database(url: URL) -> None:
    engine = get_engine(url._replace(database=None))
    async with engine.begin() as conn:
        await conn.execute(text("commit"))
        await conn.execute(text(f"CREATE DATABASE {url.database}"))


async def drop_database(url: URL) -> None:
    engine = get_engine(url._replace(database=None))
    async with engine.begin() as conn:
        await conn.execute(text("commit"))
        await conn.execute(
            text(
                """SELECT pg_terminate_backend(pg_stat_activity.pid)
                   FROM pg_stat_activity
                   WHERE pg_stat_activity.datname = :db_name
                   AND pid <> pg_backend_pid();"""
            ),
            {"db_name": url.database},
        )
        await conn.execute(text(f"DROP DATABASE {url.database}"))
