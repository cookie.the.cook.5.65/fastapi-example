import orjson
from pydantic import BaseModel

from app.utils.orjson import orjson_dumps


class BaseSchema(BaseModel):
    class Config:
        json_loads = orjson.loads
        json_dumps = orjson_dumps
