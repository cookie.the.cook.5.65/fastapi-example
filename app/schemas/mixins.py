import datetime
import uuid

from app.schemas.base import BaseSchema


class ID(BaseSchema):
    id: int


class UUID(BaseSchema):
    id: uuid.UUID


class CreatedUpdatedAt(BaseSchema):
    created_at: datetime.datetime
    updated_at: datetime.datetime
