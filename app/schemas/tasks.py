import uuid
from enum import StrEnum
from typing import Any

from app.schemas.base import BaseSchema


class SendTask(BaseSchema):  # type: ignore
    name: str
    args: list = []
    kwargs: dict = {}


class SendTaskResult(BaseSchema):
    id: uuid.UUID

    class Config:
        orm_mode = True


class TaskState(StrEnum):
    PENDING = "PENDING"
    STARTED = "STARTED"
    RETRY = "RETRY"
    FAILURE = "FAILURE"
    SUCCESS = "SUCCESS"


class TaskResult(BaseSchema):
    state: TaskState
    result: Any

    class Config:
        orm_mode = True
