from app import constants
from app.schemas.base import BaseSchema


class Detail(BaseSchema):
    detail: str


class Unauthorized(Detail):
    detail = constants.UNAUTHORIZED


class PermissionDenied(Detail):
    detail = constants.PERMISSION_DENIED


class NotFound(Detail):
    detail = constants.NOT_FOUND


class Deleted(Detail):
    detail = constants.DELETED
