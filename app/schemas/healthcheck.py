from enum import StrEnum, auto
from typing import Any, Optional

from pydantic import validator

from app.schemas.base import BaseSchema


class HealthcheckStatus(StrEnum):
    healthy = auto()
    unhealthy = auto()


class Healthcheck(BaseSchema):
    database: HealthcheckStatus
    rabbitmq: HealthcheckStatus
    redis: HealthcheckStatus
    status: Optional[HealthcheckStatus]

    @validator("status", always=True)
    def set_status(
        cls, v: HealthcheckStatus, values: dict, **kwargs: Any
    ) -> HealthcheckStatus:
        all_healthy = all(v == HealthcheckStatus.healthy for v in values.values())
        return HealthcheckStatus.healthy if all_healthy else HealthcheckStatus.unhealthy
