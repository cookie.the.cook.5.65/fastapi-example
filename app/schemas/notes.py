from app.schemas.base import BaseSchema
from app.schemas.mixins import ID, CreatedUpdatedAt
from app.schemas.users import User


class NoteBase(BaseSchema):
    title: str
    description: str
    is_public: bool


class Note(NoteBase, CreatedUpdatedAt, ID):
    owner: User

    class Config:
        orm_mode = True


class NoteCreate(NoteBase):
    pass


class NoteUpdate(NoteBase):
    pass
