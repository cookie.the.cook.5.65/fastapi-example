from fastapi_users_db_sqlalchemy import SQLAlchemyBaseUserTableUUID
from sqlalchemy.orm import relationship

from app.database import Base


class User(SQLAlchemyBaseUserTableUUID, Base):  # type: ignore
    notes = relationship("Note", back_populates="owner")

    def __str__(self) -> str:
        return self.email
