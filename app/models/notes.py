from sqlalchemy import Boolean, String
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.database import IDSafeDeleteBase
from app.models.mixins import ID, CreatedUpdatedAt, OwnerId
from app.models.users import User


class Note(CreatedUpdatedAt, OwnerId, ID, IDSafeDeleteBase):  # type: ignore
    title: Mapped[str] = mapped_column(String(100), nullable=False)
    description: Mapped[str] = mapped_column(String, nullable=False)
    is_public: Mapped[bool] = mapped_column(Boolean, default=False, nullable=False)

    owner: Mapped[User] = relationship("User", back_populates="notes")

    @property
    def is_private(self) -> bool:
        return not self.is_public

    def can_be_retrived_by(self, user: User) -> bool:
        if self.is_public:
            return True
        if self.is_private and not user:
            return False
        return super().can_be_retrived_by(user)

    def __str__(self) -> str:
        return self.title
