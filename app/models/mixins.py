import datetime

from fastapi_users_db_sqlalchemy.generics import GUID
from sqlalchemy import DateTime, ForeignKey, Integer, func
from sqlalchemy.orm import Mapped, declared_attr, mapped_column

from app.models.users import User


class ID:
    id: Mapped[int] = mapped_column(Integer, primary_key=True, index=True)


class CreatedUpdatedAt:
    created_at: Mapped[datetime.datetime] = mapped_column(
        DateTime, server_default=func.now(), nullable=False
    )
    updated_at: Mapped[datetime.datetime] = mapped_column(
        DateTime, nullable=False, server_default=func.now(), onupdate=func.now()
    )


class OwnerId:
    @declared_attr
    def owner_id(cls) -> Mapped[GUID]:
        return mapped_column(GUID, ForeignKey("user.id"), nullable=False, index=True)

    def can_be_retrived_by(self, user: User) -> bool:
        return (self.owner_id == user.id) or (user is not None and user.is_superuser)

    def can_be_updated_by(self, user: User) -> bool:
        return self.owner_id == user.id

    def can_be_deleted_by(self, user: User) -> bool:
        return (self.owner_id == user.id) or user.is_superuser
