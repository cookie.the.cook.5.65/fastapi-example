import uuid

from fastapi_filter.contrib.sqlalchemy import Filter
from pydantic import Field

from app.models.notes import Note


class NotesFilter(Filter):
    search: str | None

    class Constants(Filter.Constants):
        model = Note
        search_model_fields = ["title", "description"]


class PublicNotesFilter(NotesFilter):
    owner_id: uuid.UUID | None = Field(default=None, alias="owner")

    class Config:
        allow_population_by_field_name = True
