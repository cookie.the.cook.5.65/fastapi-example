from typing import Any, Callable, Optional

import orjson


def orjson_dumps(v: Any, *, default: Optional[Callable[[Any], Any]]) -> str:
    # orjson.dumps returns bytes, to match standard json.dumps we need to decode
    return orjson.dumps(v, default=default).decode()
