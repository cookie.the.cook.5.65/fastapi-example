import sentry_sdk

from app.core.config import settings


def init_sentry() -> None:
    if not settings.sentry_enabled:
        return

    sentry_sdk.init(**settings.sentry_kwargs)
