from typing import Type

from fastapi_pagination import Page as BasePage
from pydantic import Field

from app.core.config import settings

Page: Type[BasePage] = BasePage.with_custom_options(
    size=Field(settings.pagination_page_size, ge=1, le=100),
)
