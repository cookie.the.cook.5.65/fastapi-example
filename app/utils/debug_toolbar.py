from typing import Any

from debug_toolbar.middleware import DebugToolbarMiddleware as _DebugToolbarMiddleware
from debug_toolbar.settings import DebugToolbarSettings
from debug_toolbar.utils import import_string
from fastapi import APIRouter
from starlette.routing import NoMatchFound
from starlette.types import ASGIApp


class DebugToolbarMiddleware(_DebugToolbarMiddleware):
    def __init__(self, app: ASGIApp, **settings: Any) -> None:
        super(_DebugToolbarMiddleware, self).__init__(app)
        self.settings = DebugToolbarSettings(**settings)
        self.show_toolbar = import_string(self.settings.SHOW_TOOLBAR_CALLBACK)
        self.router: APIRouter = app  # type: ignore[assignment]

        while not isinstance(self.router, APIRouter):
            self.router = self.router.app
        try:
            # changed:
            # self.router.url_path_for(name="debug_toolbar.render_panel")
            self.router.url_path_for(  # type: ignore
                _Router__name="debug_toolbar.render_panel"
            )
        except NoMatchFound:
            self.init_toolbar()
