from fastapi import FastAPI
from prometheus_fastapi_instrumentator import Instrumentator

from app.core.config import settings


def add_metrics(app: FastAPI) -> None:
    if settings.metrics_enabled:
        Instrumentator().instrument(app).expose(app)
