from fastapi import HTTPException, status

from app import constants


class NotFound(HTTPException):
    def __init__(self) -> None:
        super().__init__(
            status_code=status.HTTP_404_NOT_FOUND, detail=constants.NOT_FOUND
        )


class PermissionDenied(HTTPException):
    def __init__(self) -> None:
        super().__init__(
            status_code=status.HTTP_403_FORBIDDEN, detail=constants.PERMISSION_DENIED
        )
