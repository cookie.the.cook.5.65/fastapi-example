from sqladmin import ModelView

from app.models.notes import Note


class NoteAdmin(ModelView, model=Note):
    column_list = [Note.id, Note.title, Note.description, Note.is_public]
    form_columns = [Note.title, Note.description, Note.is_public, Note.deleted_at]
    column_details_list = [
        Note.id,
        Note.title,
        Note.description,
        Note.is_public,
        Note.created_at,
        Note.updated_at,
        Note.deleted_at,
        Note.owner,
    ]
