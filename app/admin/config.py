from fastapi import FastAPI
from sqladmin import Admin

from app.admin.auth import AdminAuth
from app.admin.notes import NoteAdmin
from app.admin.users import UserAdmin  # type: ignore
from app.core.config import settings
from app.database import engine


def add_admin(app: FastAPI) -> Admin:
    authentication_backend = AdminAuth(secret_key=settings.secret.get_secret_value())
    admin = Admin(app=app, engine=engine, authentication_backend=authentication_backend)

    admin.add_view(UserAdmin)
    admin.add_view(NoteAdmin)

    return admin
