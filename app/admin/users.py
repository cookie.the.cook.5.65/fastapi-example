# type: ignore
from sqladmin import ModelView

from app.models.users import User


class UserAdmin(ModelView, model=User):
    column_list = [
        User.id,
        User.email,
        User.is_active,
        User.is_verified,
        User.is_superuser,
    ]
    form_columns = [User.email, User.is_active, User.is_verified, User.is_superuser]
    column_details_list = [
        User.id,
        User.email,
        User.is_active,
        User.is_verified,
        User.is_superuser,
    ]
