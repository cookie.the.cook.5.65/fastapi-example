from fastapi import Response
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_users_db_sqlalchemy import SQLAlchemyUserDatabase
from sqladmin.authentication import AuthenticationBackend
from starlette.requests import Request
from starlette.responses import RedirectResponse

from app.database.session import session_factory
from app.models.users import User
from app.users import UserManager, auth_backend, get_jwt_strategy


class AdminAuth(AuthenticationBackend):
    async def login(self, request: Request) -> bool:
        form = await request.form()
        username, password = str(form["username"]), str(form["password"])
        async with session_factory() as session:
            user_manager = UserManager(SQLAlchemyUserDatabase(session, User))
            user = await user_manager.authenticate(
                credentials=OAuth2PasswordRequestForm(
                    username=username, password=password, scope=""
                )
            )
        if not user or not user.is_superuser:
            return False
        token = await auth_backend.login(
            strategy=get_jwt_strategy(), user=user, response=Response()
        )
        request.session.update({"token": token.access_token})
        return True

    async def logout(self, request: Request) -> bool:
        request.session.clear()
        return True

    async def authenticate(self, request: Request) -> RedirectResponse | None:
        token = request.session.get("token")
        if not token:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)

        async with session_factory() as session:
            user_manager = UserManager(SQLAlchemyUserDatabase(session, User))
            user = await get_jwt_strategy().read_token(
                token=token, user_manager=user_manager
            )
        if not user:
            return RedirectResponse(request.url_for("admin:login"), status_code=302)
        return None
