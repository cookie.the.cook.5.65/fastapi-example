from celery.result import AsyncResult
from fastapi import APIRouter, Depends

from app import schemas
from app.celery.app import celery_app
from app.dependencies.users import get_superuser

router = APIRouter()


@router.post(
    "",
    name="tasks:send_task",
    response_model=schemas.SendTaskResult,
    dependencies=[Depends(get_superuser)],
)
async def send_task(task: schemas.SendTask) -> dict:
    return celery_app.send_task(**task.dict())


@router.get(
    "/{task_id}",
    name="tasks:get_task_result",
    response_model=schemas.TaskResult,
)
async def get_task_result(task_id: str) -> dict:
    return AsyncResult(id=task_id, app=celery_app)
