from fastapi import APIRouter, status
from fastapi_filter import FilterDepends

from app import constants, exceptions, models, schemas
from app.dependencies.notes import NoteDep, NoteServiceDep
from app.dependencies.users import UserDep, UserOrNoneDep
from app.filters.notes import NotesFilter, PublicNotesFilter
from app.utils.pagination import Page

router = APIRouter()


@router.get(
    "/feed",
    name="notes:list_public_notes",
    response_model=Page[schemas.Note],  # type: ignore
)
async def list_public_notes(
    note_service: NoteServiceDep,
    notes_filter: PublicNotesFilter = FilterDepends(PublicNotesFilter),
) -> dict:
    return await note_service.get_public_notes(notes_filter=notes_filter)


@router.get(
    "",
    response_model=Page[schemas.Note],  # type: ignore
    responses={
        status.HTTP_401_UNAUTHORIZED: {
            "model": schemas.Unauthorized,
            "description": constants.UNAUTHORIZED,
        },
        status.HTTP_403_FORBIDDEN: {
            "model": schemas.PermissionDenied,
            "description": constants.PERMISSION_DENIED,
        },
    },
    name="notes:list_own_notes",
)
async def list_own_notes(
    user: UserDep,
    note_service: NoteServiceDep,
    notes_filter: NotesFilter = FilterDepends(NotesFilter),
) -> dict:
    return await note_service.get_own_notes(notes_filter=notes_filter, user=user)


@router.post(
    "",
    response_model=schemas.Note,
    status_code=status.HTTP_201_CREATED,
    responses={
        status.HTTP_401_UNAUTHORIZED: {
            "model": schemas.Unauthorized,
            "description": constants.UNAUTHORIZED,
        },
        status.HTTP_403_FORBIDDEN: {
            "model": schemas.PermissionDenied,
            "description": constants.PERMISSION_DENIED,
        },
    },
    name="notes:create_note",
)
async def create_note(
    note: schemas.NoteCreate,
    user: UserDep,
    note_service: NoteServiceDep,
) -> models.Note:
    return await note_service.create(note=note, user=user)


@router.get(
    "/{note_id}",
    response_model=schemas.Note,
    responses={
        status.HTTP_404_NOT_FOUND: {
            "model": schemas.NotFound,
            "description": constants.NOT_FOUND,
        },
        status.HTTP_403_FORBIDDEN: {
            "model": schemas.PermissionDenied,
            "description": constants.PERMISSION_DENIED,
        },
    },
    name="notes:retrive_note",
)
async def retrive_note(
    user: UserOrNoneDep,
    note: NoteDep,
) -> models.Note:
    if not note.can_be_retrived_by(user=user):
        raise exceptions.http.PermissionDenied
    return note


@router.patch(
    "/{note_id}",
    response_model=schemas.Note,
    responses={
        status.HTTP_401_UNAUTHORIZED: {
            "model": schemas.Unauthorized,
            "description": constants.UNAUTHORIZED,
        },
        status.HTTP_403_FORBIDDEN: {
            "model": schemas.PermissionDenied,
            "description": constants.PERMISSION_DENIED,
        },
        status.HTTP_404_NOT_FOUND: {
            "model": schemas.NotFound,
            "description": constants.NOT_FOUND,
        },
    },
    name="notes:update_note",
)
async def update_note(
    note: schemas.NoteUpdate,
    user: UserDep,
    db_note: NoteDep,
    note_service: NoteServiceDep,
) -> models.Note:
    if not db_note.can_be_updated_by(user=user):
        raise exceptions.http.PermissionDenied
    return await note_service.update(db_note=db_note, note_update=note)


@router.delete(
    "/{note_id}",
    response_model=schemas.Deleted,
    responses={
        status.HTTP_401_UNAUTHORIZED: {
            "model": schemas.Unauthorized,
            "description": constants.UNAUTHORIZED,
        },
        status.HTTP_403_FORBIDDEN: {
            "model": schemas.PermissionDenied,
            "description": constants.PERMISSION_DENIED,
        },
        status.HTTP_404_NOT_FOUND: {
            "model": schemas.NotFound,
            "description": constants.NOT_FOUND,
        },
    },
    name="notes:delete_note",
)
async def delete_note(
    user: UserDep,
    note: NoteDep,
    note_service: NoteServiceDep,
) -> dict:
    if not note.can_be_deleted_by(user=user):
        raise exceptions.http.PermissionDenied
    await note_service.delete(pk=note.id)
    return {"detail": constants.DELETED}
