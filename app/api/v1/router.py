from fastapi import APIRouter

from app.api.v1 import notes, tasks
from app.schemas import User, UserCreate, UserUpdate
from app.users import auth_backend, fastapi_users

router = APIRouter()
router.include_router(notes.router, prefix="/notes", tags=["notes"])
router.include_router(tasks.router, prefix="/tasks", tags=["tasks"])
router.include_router(
    fastapi_users.get_auth_router(auth_backend), prefix="/auth/jwt", tags=["auth"]
)
router.include_router(
    fastapi_users.get_register_router(User, UserCreate),
    prefix="/auth",
    tags=["auth"],
)
router.include_router(
    fastapi_users.get_reset_password_router(),
    prefix="/auth",
    tags=["auth"],
)
router.include_router(
    fastapi_users.get_verify_router(User),
    prefix="/auth",
    tags=["auth"],
)
router.include_router(
    fastapi_users.get_users_router(User, UserUpdate),
    prefix="/users",
    tags=["users"],
)
# make /users/{id} public
for route in router.routes:
    if route.name == "users:user":  # type: ignore
        route.dependencies = []  # type: ignore
