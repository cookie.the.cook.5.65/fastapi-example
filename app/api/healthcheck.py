from fastapi import APIRouter

from app.dependencies.healthcheck import HealthcheckServiceDep
from app.schemas.healthcheck import Healthcheck

router = APIRouter()


@router.get(
    "/healthcheck",
    response_model=Healthcheck,
    name="healthcheck",
    tags=["healthcheck"],
)
async def healthcheck(healthcheck_service: HealthcheckServiceDep) -> dict:
    return await healthcheck_service.check()
