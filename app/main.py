from app.core.app import create_app
from app.core.config import settings

app = create_app(settings)
