from typing import Annotated

from fastapi import Depends
from redis import Redis

from app.core.config import settings
from app.dependencies.database import DBDep
from app.services.healthcheck import HealthcheckService


def get_healthcheck_service(db: DBDep) -> HealthcheckService:
    return HealthcheckService(
        db=db, redis=Redis.from_url(settings.celery_result_backend)
    )


HealthcheckServiceDep = Annotated[HealthcheckService, Depends(get_healthcheck_service)]
