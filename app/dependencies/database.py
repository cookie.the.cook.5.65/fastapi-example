from typing import Annotated, AsyncIterator

from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession

from app.database import session_factory


async def get_db() -> AsyncIterator[AsyncSession]:
    async with session_factory() as session:
        yield session


DBDep = Annotated[AsyncSession, Depends(get_db)]
