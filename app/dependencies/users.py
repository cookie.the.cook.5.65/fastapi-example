from typing import Annotated

from fastapi import Depends

from app.models.users import User
from app.users import fastapi_users

get_user = fastapi_users.current_user(active=True)
get_user_or_none = fastapi_users.current_user(active=True, optional=True)
get_superuser = fastapi_users.current_user(active=True, superuser=True)

UserDep = Annotated[User, Depends(get_user)]
UserOrNoneDep = Annotated[User, Depends(get_user_or_none)]
SuperuserDep = Annotated[User, Depends(get_superuser)]
