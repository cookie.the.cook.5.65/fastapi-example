from typing import Annotated, AsyncIterator

from fastapi import Depends

from app import exceptions
from app.dependencies.database import DBDep
from app.models.notes import Note
from app.repositories.notes import NotesRepository
from app.services.notes import NoteService


def get_notes_repo(db: DBDep) -> NotesRepository:
    return NotesRepository(db=db)


NotesRepoDep = Annotated[NotesRepository, Depends(get_notes_repo)]


async def get_note_service(
    db: DBDep, notes_repo: NotesRepoDep
) -> AsyncIterator[NoteService]:
    yield NoteService(db=db, notes_repository=notes_repo)


NoteServiceDep = Annotated[NoteService, Depends(get_note_service)]


async def get_note(note_id: int, note_service: NoteServiceDep) -> Note:
    note = await note_service.get(pk=note_id)
    if note is None:
        raise exceptions.http.NotFound
    return note


NoteDep = Annotated[Note, Depends(get_note)]
