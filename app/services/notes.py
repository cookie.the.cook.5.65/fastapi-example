from typing import Literal

from fastapi_pagination.ext.sqlalchemy import paginate
from sqlalchemy.ext.asyncio import AsyncSession

from app.filters.notes import NotesFilter
from app.models.notes import Note
from app.models.users import User
from app.repositories.notes import NotesRepository
from app.schemas.notes import NoteCreate, NoteUpdate


class NoteService:
    def __init__(self, db: AsyncSession, notes_repository: NotesRepository) -> None:
        self.db = db
        self.repository = notes_repository

    async def get_public_notes(self, notes_filter: NotesFilter) -> dict:
        query = self.repository.get_public_notes_query()
        query = notes_filter.filter(query)
        return await paginate(conn=self.db, query=query)

    async def get_own_notes(self, notes_filter: NotesFilter, user: User) -> dict:
        query = self.repository.get_owner_notes_query(owner_id=user.id)
        query = notes_filter.filter(query)
        return await paginate(conn=self.db, query=query)

    async def create(self, note: NoteCreate, user: User) -> Note:
        return await self.repository.create(fields={**note.dict(), "owner_id": user.id})

    async def get(self, pk: int) -> Note | None:
        return await self.repository.get(pk=pk)

    async def update(self, db_note: Note, note_update: NoteUpdate) -> Note:
        return await self.repository.update(
            db_obj=db_note, fields=note_update.dict(exclude_unset=True)
        )

    async def delete(self, pk: int) -> Literal[True]:
        return await self.repository.delete(pk=pk)
