import asyncio
import functools
import logging
from typing import Any, Callable

import kombu
from redis import Redis
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession
from starlette._utils import is_async_callable

from app.core.config import settings
from app.schemas.healthcheck import HealthcheckStatus

logger = logging.getLogger("healthcheck")


def healthchecker(fn: Callable) -> Callable:
    @functools.wraps(fn)
    async def healthchecker_wrapper(*args: Any, **kwargs: Any) -> HealthcheckStatus:
        if is_async_callable(fn):
            checker = fn(*args, **kwargs)
        else:
            checker = asyncio.to_thread(fn, *args, **kwargs)

        try:
            await checker
        except Exception as e:
            logger.exception(e)
            status = HealthcheckStatus.unhealthy
        else:
            status = HealthcheckStatus.healthy
        return status

    return healthchecker_wrapper


class HealthcheckService:
    def __init__(self, db: AsyncSession, redis: Redis) -> None:
        self.db = db
        self.redis = redis

    async def check(self) -> dict:
        services = ["database", "rabbitmq", "redis"]
        results = await asyncio.gather(
            self.check_database(),
            self.check_rabbitmq(),
            self.check_redis(),
        )
        return dict(zip(services, results))

    @healthchecker
    async def check_database(self) -> None:
        await self.db.execute(select(1))

    @healthchecker
    def check_rabbitmq(self) -> None:
        conn = kombu.Connection(settings.celery_broker_url)
        conn.connect()
        conn.release()

    @healthchecker
    def check_redis(self) -> None:
        self.redis.ping()
