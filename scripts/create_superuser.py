#!/usr/bin/env python
import sys
from getpass import getpass
from pathlib import Path

sys.path.append(str(Path(__file__).parent.parent))

import asyncio  # noqa: E402

from fastapi_users_db_sqlalchemy import SQLAlchemyUserDatabase  # noqa: E402

from app.database import session_factory  # noqa: E402
from app.models import User  # noqa: E402
from app.schemas import UserCreate  # noqa: E402
from app.users import UserManager  # noqa: E402


async def main() -> None:
    email = input("Email: ")
    password = getpass("Password: ")

    user = UserCreate(
        email=email,
        password=password,
        is_active=True,
        is_superuser=True,
        is_verified=True,
    )

    async with session_factory() as db:
        user_manager = UserManager(SQLAlchemyUserDatabase(db, User))
        await user_manager.create(user)

    print("Superuser created")


if __name__ == "__main__":
    asyncio.run(main())
