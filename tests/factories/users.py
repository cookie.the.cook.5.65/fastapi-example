import factory

from app.models.users import User
from tests.factories.base import AsyncFactory


class UserFactory(AsyncFactory):
    id = factory.Faker("uuid4", cast_to=None)
    email = factory.Faker("email")
    hashed_password = factory.Faker("sha256")
    is_active = True
    is_superuser = False
    is_verified = factory.Faker("boolean")

    class Meta:
        model = User
