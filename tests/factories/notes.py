import factory

from app.models.notes import Note
from tests.factories.base import AsyncFactory


class NoteFactory(AsyncFactory):
    id = factory.Sequence(lambda n: n)
    title = factory.Faker("sentence", nb_words=3)
    description = factory.Faker("text")
    is_public = factory.Faker("boolean")
    created_at = factory.Faker("past_datetime")
    updated_at = factory.Faker("past_datetime")
    deleted_at = None
    owner_id = None

    class Meta:
        model = Note


class CreateNoteDictFactory(factory.DictFactory):
    title = factory.Faker("sentence", nb_words=3)
    description = factory.Faker("text")
    is_public = factory.Faker("boolean")


class UpdateNoteDictFactory(CreateNoteDictFactory):
    pass
