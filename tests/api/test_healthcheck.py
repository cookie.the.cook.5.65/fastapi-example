from fastapi import FastAPI
from httpx import AsyncClient

from app import schemas


async def test_healthcheck(app: FastAPI, client: AsyncClient) -> None:
    response = await client.get(app.url_path_for("healthcheck"))
    content = response.json()
    assert content["database"] == schemas.HealthcheckStatus.healthy
    assert content["rabbitmq"] == schemas.HealthcheckStatus.healthy
    assert content["redis"] == schemas.HealthcheckStatus.healthy
    assert content["status"] == schemas.HealthcheckStatus.healthy
