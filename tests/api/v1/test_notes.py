import datetime

from fastapi import FastAPI, status
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import User
from tests.factories.notes import (
    CreateNoteDictFactory,
    NoteFactory,
    UpdateNoteDictFactory,
)


class TestListPublicNotes:
    async def test_unauthorized_user_cant_list_private_notes(
        self,
        app: FastAPI,
        client: AsyncClient,
        another_user: User,
    ) -> None:
        await NoteFactory(owner_id=another_user.id, is_public=False)

        response = await client.get(app.url_path_for("notes:list_public_notes"))
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["total"] == 0

    async def test_unauthorized_user_can_list_public_notes(
        self,
        app: FastAPI,
        client: AsyncClient,
        another_user: User,
    ) -> None:
        await NoteFactory(owner_id=another_user.id, is_public=True)

        response = await client.get(app.url_path_for("notes:list_public_notes"))
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["total"] == 1


class TestListOwnNotes:
    async def test_unauthorized_user_cant_list_notes(
        self, app: FastAPI, client: AsyncClient
    ) -> None:
        response = await client.get(app.url_path_for("notes:list_own_notes"))

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_user_cant_list_another_user_notes(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        another_user: User,
    ) -> None:
        await NoteFactory(owner_id=another_user.id, is_public=True)

        response = await authorized_client.get(app.url_path_for("notes:list_own_notes"))
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["total"] == 0

    async def test_user_can_list_his_notes(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        user: User,
    ) -> None:
        await NoteFactory(owner_id=user.id, is_public=True)
        await NoteFactory(owner_id=user.id, is_public=False)

        response = await authorized_client.get(app.url_path_for("notes:list_own_notes"))
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["total"] == 2


class TestRetriveNote:
    async def test_unauthorized_user_can_retrive_public_note(
        self,
        app: FastAPI,
        client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id, is_public=True)

        response = await client.get(
            app.url_path_for("notes:retrive_note", note_id=note.id)
        )
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["id"] == note.id
        assert content["title"] == note.title
        assert content["description"] == note.description
        assert content["is_public"] == note.is_public
        assert content["owner"]["id"] == str(note.owner_id)
        assert "deleted_at" not in content

    async def test_unauthorized_user_cant_retrive_private_note(
        self,
        app: FastAPI,
        client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id, is_public=False)

        response = await client.get(
            app.url_path_for("notes:retrive_note", note_id=note.id)
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    async def test_user_cant_retrive_not_existing_note(
        self, app: FastAPI, client: AsyncClient
    ) -> None:
        response = await client.get(app.url_path_for("notes:retrive_note", note_id=1))

        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_user_cant_retrive_deleted_note(
        self, app: FastAPI, client: AsyncClient, user: User
    ) -> None:
        note = await NoteFactory(
            owner_id=user.id, is_public=True, deleted_at=datetime.datetime.now()
        )

        response = await client.get(
            app.url_path_for("notes:retrive_note", note_id=note.id)
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_user_can_retrive_his_private_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        user: User,
    ) -> None:
        note = await NoteFactory(owner_id=user.id, is_public=False)

        response = await authorized_client.get(
            app.url_path_for("notes:retrive_note", note_id=note.id)
        )
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["id"] == note.id
        assert content["title"] == note.title
        assert content["description"] == note.description
        assert content["is_public"] == note.is_public
        assert content["owner"]["id"] == str(note.owner_id)
        assert "deleted_at" not in content

    async def test_user_cant_retrive_not_his_private_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(is_public=False, owner_id=another_user.id)

        response = await authorized_client.get(
            app.url_path_for("notes:retrive_note", note_id=note.id)
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    async def test_superuser_can_retrive_not_his_private_note(
        self,
        app: FastAPI,
        superuser_client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(is_public=False, owner_id=another_user.id)

        response = await superuser_client.get(
            app.url_path_for("notes:retrive_note", note_id=note.id)
        )
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["id"] == note.id
        assert content["title"] == note.title
        assert content["description"] == note.description
        assert content["is_public"] == note.is_public
        assert content["owner"]["id"] == str(note.owner_id)
        assert "deleted_at" not in content


class TestCreateNote:
    async def test_unauthorized_user_cant_create_note(
        self, app: FastAPI, client: AsyncClient
    ) -> None:
        note = CreateNoteDictFactory()
        response = await client.post(app.url_path_for("notes:create_note"), json=note)

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_user_can_create_note(
        self, app: FastAPI, authorized_client: AsyncClient, user: User
    ) -> None:
        note = CreateNoteDictFactory()
        response = await authorized_client.post(
            app.url_path_for("notes:create_note"), json=note
        )
        content = response.json()

        assert response.status_code == status.HTTP_201_CREATED
        assert "id" in content
        assert content["title"] == note["title"]
        assert content["description"] == note["description"]
        assert content["is_public"] == note["is_public"]
        assert content["owner"]["id"] == str(user.id)
        assert content["updated_at"] == content["created_at"]
        assert "deleted_at" not in content


class TestUpdateNote:
    async def test_unauthorized_user_cant_update_note(
        self,
        app: FastAPI,
        client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id)

        response = await client.patch(
            app.url_path_for("notes:update_note", note_id=note.id),
            json=UpdateNoteDictFactory(),
        )

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_user_cant_update_not_his_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id)

        response = await authorized_client.patch(
            app.url_path_for("notes:update_note", note_id=note.id),
            json=UpdateNoteDictFactory(),
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    async def test_superuser_cant_update_not_his_note(
        self,
        app: FastAPI,
        superuser_client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id)

        response = await superuser_client.patch(
            app.url_path_for("notes:update_note", note_id=note.id),
            json=UpdateNoteDictFactory(),
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    async def test_user_cant_update_deleted_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        user: User,
    ) -> None:
        note = await NoteFactory(owner_id=user.id, deleted_at=datetime.datetime.now())

        response = await authorized_client.patch(
            app.url_path_for("notes:update_note", note_id=note.id),
            json=UpdateNoteDictFactory(),
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_user_can_update_his_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        user: User,
    ) -> None:
        note = await NoteFactory(owner_id=user.id)

        response = await authorized_client.patch(
            app.url_path_for("notes:update_note", note_id=note.id),
            json=UpdateNoteDictFactory(),
        )
        content = response.json()

        assert response.status_code == status.HTTP_200_OK
        assert content["id"] == note.id
        assert content["title"] == note.title
        assert content["description"] == note.description
        assert content["is_public"] == note.is_public
        assert content["owner"]["id"] == str(user.id)
        assert content["created_at"] == note.created_at.isoformat()
        assert content["updated_at"] >= content["created_at"]
        assert "deleted_at" not in content


class TestDeleteNote:
    async def test_unauthorized_user_cant_delete_note(
        self,
        app: FastAPI,
        client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id)

        response = await client.delete(
            app.url_path_for("notes:delete_note", note_id=note.id)
        )

        assert response.status_code == status.HTTP_401_UNAUTHORIZED

    async def test_user_cant_delete_not_his_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id)

        response = await authorized_client.delete(
            app.url_path_for("notes:delete_note", note_id=note.id)
        )

        assert response.status_code == status.HTTP_403_FORBIDDEN

    async def test_user_cant_delete_not_existing_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
    ) -> None:
        response = await authorized_client.delete(
            app.url_path_for("notes:delete_note", note_id=1)
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_user_cant_delete_deleted_note(
        self,
        app: FastAPI,
        authorized_client: AsyncClient,
        user: User,
    ) -> None:
        note = await NoteFactory(owner_id=user.id, deleted_at=datetime.datetime.now())

        response = await authorized_client.delete(
            app.url_path_for("notes:delete_note", note_id=note.id)
        )

        assert response.status_code == status.HTTP_404_NOT_FOUND

    async def test_user_can_delete_his_note(
        self,
        app: FastAPI,
        db: AsyncSession,
        authorized_client: AsyncClient,
        user: User,
    ) -> None:
        note = await NoteFactory(owner_id=user.id, deleted_at=None)

        response = await authorized_client.delete(
            app.url_path_for("notes:delete_note", note_id=note.id)
        )
        await db.refresh(note)

        assert response.status_code == status.HTTP_200_OK
        assert note.deleted_at is not None

    async def test_superuser_can_delete_not_his_note(
        self,
        app: FastAPI,
        db: AsyncSession,
        superuser_client: AsyncClient,
        another_user: User,
    ) -> None:
        note = await NoteFactory(owner_id=another_user.id)

        response = await superuser_client.delete(
            app.url_path_for("notes:delete_note", note_id=note.id)
        )
        await db.refresh(note)

        assert response.status_code == status.HTTP_200_OK
        assert note.deleted_at is not None
