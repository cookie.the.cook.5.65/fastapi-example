import asyncio
from typing import AsyncIterator, Iterator

import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from sqlalchemy.ext.asyncio import AsyncEngine, AsyncSession

from app.core.config import settings
from app.database import Base, get_engine, get_session_factory
from app.database.utils import create_database, drop_database
from app.dependencies.database import get_db
from app.main import create_app
from app.models import User
from tests import constants
from tests.factories.notes import NoteFactory
from tests.factories.users import UserFactory
from tests.utils.auth import get_auth_headers


@pytest.fixture(scope="session")
def event_loop() -> Iterator[asyncio.AbstractEventLoop]:
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
def app() -> FastAPI:
    return create_app(settings)


@pytest.fixture(scope="session")
async def db_engine() -> AsyncIterator[AsyncEngine]:
    engine = get_engine(url=f"{settings.database_url}{constants.TEST_DB_POSTFIX}")

    await create_database(engine.url)

    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)

    yield engine

    await drop_database(engine.url)


@pytest.fixture
async def db(app: FastAPI, db_engine: AsyncEngine) -> AsyncIterator[AsyncSession]:
    session_factory = get_session_factory(engine=db_engine)

    async with (
        db_engine.connect() as connection,
        connection.begin() as transaction,
    ):
        async with session_factory(bind=connection) as session:
            app.dependency_overrides[get_db] = lambda: session
            yield session
        await transaction.rollback()


@pytest.fixture
async def alembic_engine() -> AsyncIterator[AsyncEngine]:
    engine = get_engine(
        url=f"{settings.database_url}{constants.TEST_MIGRATIONS_DB_POSTFIX}"
    )

    await create_database(engine.url)

    yield engine

    await drop_database(engine.url)


@pytest.fixture(autouse=True)
def set_factories_session(db: AsyncSession) -> None:
    NoteFactory._meta.sqlalchemy_session = db
    UserFactory._meta.sqlalchemy_session = db


@pytest.fixture
async def user() -> AsyncIterator[User]:
    return await UserFactory()


@pytest.fixture
async def another_user() -> AsyncIterator[User]:
    return await UserFactory()


@pytest.fixture
async def superuser() -> AsyncIterator[User]:
    return await UserFactory(is_superuser=True)


@pytest.fixture
async def client(app: FastAPI) -> AsyncIterator[AsyncClient]:
    async with AsyncClient(app=app, base_url=constants.TEST_BASE_URL) as client:
        yield client


@pytest.fixture
async def authorized_client(app: FastAPI, user: User) -> AsyncIterator[AsyncClient]:
    async with AsyncClient(
        app=app,
        base_url=constants.TEST_BASE_URL,
        headers=await get_auth_headers(user=user),
    ) as client:
        yield client


@pytest.fixture
async def superuser_client(app: FastAPI, superuser: User) -> AsyncIterator[AsyncClient]:
    async with AsyncClient(
        app=app,
        base_url=constants.TEST_BASE_URL,
        headers=await get_auth_headers(user=superuser),
    ) as client:
        yield client
