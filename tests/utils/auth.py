from typing import Dict

from fastapi import Response

from app import models
from app.users import auth_backend, get_jwt_strategy


async def get_auth_headers(user: models.User) -> Dict[str, str]:
    token = await auth_backend.login(
        strategy=get_jwt_strategy(), user=user, response=Response()
    )
    return {"Authorization": f"Bearer {token.access_token}"}
